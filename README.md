# Picasso - Thread-based Image Processing Library #

The library supports Thread-based Image Processing and Center Crop. 

Theading is very useful for uploading an image to S3 especially. 


### Installation ###

    pip install anderson.picasso

### Synchronous Example ###

    picasso = Picasso()
    picasso.open('image.jpg').resize(200, 200).execute()
    picasso.save('test/resized01.jpg')

### Asynchronous Example ###

    def callback(picasso):
        print picasso.size

    picasso = Picasso()
    picasso.open('image.jpg')

    manager = PicassoManager()
    manager.put(picasso, callback)
    manager.join()

### Synchronous Center Crop ###

    picasso = Picasso()
    picasso.open('image.jpg').centerCrop(True).resize(200, 200)
    picasso.execute() # synchronous execution

### Asynchronous Center Crop ###

    def callback(picasso):
        picasso.save('result.jpg')

    picasso = Picasso()
    picasso.open('image.jpg').centerCrop(True).resize(200, 200)

    manager = PicassoManager()
    manager.put(picasso, callback)

### Get PicassoManager Singleton ###

In production, do not use PicassoManager class directly. 

Always use 'get_picasso_manager' method. 

    manager = get_picasso_manager()

### Best Practice ###

The Picasso instance do not trigger any cpu-intensive processing like centerCrop or resize methods. 

In Synchronous environment, you can run 'execute' method to trigger image processing like centerCrop or resize. 

In Asychronous environment, you can use the 'put' method of PicassoManager class. 

The 'put' method automatically put the picasso image into the thread and run the thread through Queue. 

피카소는 centerCrop, resize등등의 함수를 할때, 실제 이미지 프로세싱이 일어나지 않는다.

sync에서는 execute 함수가 실행이 될때 그때 모든 이미지 프로세싱이 실행이 되며, 

async에서는 PicassoManager의 put 에 넣을때 자동으로 쓰레드로 돌아가게 된다. 

만약 많은 양의 put이 실행될경우 단일 쓰레드에서 Queue통해서 처리가 된다. 

### Who do I talk to? ###

* Anderson (조창민) a141890@gmail.com