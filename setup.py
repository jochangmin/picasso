from distutils.core import setup

setup(
    name='anderson.picasso',
    version='0.4.5',
    packages=['test', 'picasso'],
    url='https://bitbucket.org/jochangmin/picasso',
    download_url='https://bitbucket.org/jochangmin/picasso/get/0.1.tar.gz',
    license='MIT',
    author='Anderson',
    author_email='a141890@gmail.com',
    description='Image Processing + Thread + AWS',
    keywords=['Image Processing', 'Asynchronous']
)
